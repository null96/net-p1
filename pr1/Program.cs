﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pr1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("0_o Hello world! :)");
            Console.ReadLine();
            Factorial();
            MaxItem();
            swapArrayItems();
            Console.WriteLine("NOD of 3045 & 256 = " + nod(3045, 256));
            Console.ReadLine();
        }
        static void Factorial()
        {
            for (int i = 2; i <= 13; ++i)
            {
                int res = 1;
                for (int k = i; k > 1; --k)
                {
                    res *= k;
                }
                Console.WriteLine(i + "! = " + res);
            }
            Console.ReadLine();
        }
        static void MaxItem()
        {
            int[] arr = { 4, 1, 23, 5, 12, 4, 1, 67, 12 };
            int max = arr[0];
            for (int i = 1; i < arr.Length; ++i)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                }
            }
            Console.WriteLine("Max value in the array is " + max);
            Console.ReadLine();
        }
        static void swapArrayItems() {
            int[] arr = { 4, 1 };
            Console.WriteLine("Before " + arr[0] + ',' + arr[1]);
            int tmp = arr[0];
            arr[0] = arr[1];
            arr[1] = tmp;
            Console.WriteLine("After " + arr[0] + ',' + arr[1]);
            Console.ReadLine();
        }
        static int nod(int x, int y)
        {
            while (x != y)
            {
                if (x > y)
                    x = x - y;
                else
                    y = y - x;
            }
            return x;
        }
    }
}
